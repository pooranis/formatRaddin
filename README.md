



June 17, 2022

#  DESCRIPTION

```
Package: formatRaddin
Title: formatR addin
Version: 1.0
Authors@R (parsed):
    * Poorani Subramanian <first.last@example.com> [aut, cre] (YOUR-ORCID-ID)
Description: Formats selection with formatR tidy_rstudio.
License: MIT + file LICENSE
Imports:
    formatR,
    rstudioapi
Encoding: UTF-8
Roxygen: list(markdown = TRUE)
RoxygenNote: 7.2.0
```


# `formatRaddin`

formatR Addins

## Description

RStudio addins to format R code.  Look under **Addins** menu for *FORMATRADDIN* .
 Those items will run these functions on selected text in your R document.


## Usage

```r
formatRaddin()
spinComment()
```


## Addins

*  `Format Selection` Formats using tidy_source - see that doc for options to control formatting.  
*  `Spin Comment Selection` Comments selected code using custom comment char that can be set getOption('formatR.commentchar', default="#'") .
 By default, it uses a roxygen/spin comment #' which will be converted to text when rendering a script.




